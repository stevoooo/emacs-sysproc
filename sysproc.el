;;; sysproc.el --- dsplay system processes  -*- lexical-binding: t -*-
;;
;; Copyright (C) 2021  Free Software Foundation, Inc.
;;
;; Author: Steve Perry <stevoooo@gmail.com>
;; URL: https://gitlab.com/stevoooo/emacs-sysproc
;; Version: 0.1
;;
;; This file is part of GNU Emacs.
;;
;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; For a full copy of the GNU General Public License
;; see <https://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Allow user to display and send signals to system processes in the
;; manner of dired.
;;
;;; Code:

(defvar sysproc-buffer-name "*System Processes*"
  "Name of buffer to be created by `sysproc'.")

(defvar sysproc-default-signal 'SIGINT
  "Signal to be sent to processes by `sysproc-do-marked-delete'.
Use `sysproc-do-marked-delete' with a prefix argument to pick the
signal to be sent from those in `sysproc-signal-list'.")

(defvar sysproc-sort-field nil
  "Default sort field for the process list.
Either nil in which case the list is unsorted and the order
matches that returned by `list-system-processes', or a numeric
index into `sysproc-tabulated-list-format' specifying the field
on which the list should be sorted.  See `sysproc-toggle-sort'.")

(defvar sysproc-signal-list
  '(SIGHUP SIGINT SIGQUIT SIGILL SIGABRT SIGFPE SIGKILL SIGSEGV
    SIGPIPE SIGALRM SIGTERM SIGUSR1 SIGUSR2 SIGCHLD SIGCONT SIGSTOP
    SIGTSTP SIGTTIN SIGTTOU)
  "Signals which may be sent by `sysproc-do-marked-delete'.
The signals specified here come from the \"Standard signals\" section
of the signal(7) man page on GNU/Linux.")

(defun sysproc-make-numeric-sort-fn (column)
  "Return numeric comparison function for COLUMN.
Values are sorted to ensure that the largest items appear first.
See `sysproc--get-process-list' for details of each row."
  #'(lambda (a b)
      "Compare A and B as required for `tabulated-list-format'."
      (let ((entry-a (string-to-number (elt (cadr a) column)))
	    (entry-b (string-to-number (elt (cadr b) column))))
	(> entry-a entry-b))))

(defvar sysproc-tabulated-list-format
  `[("PID" 8 nil)
    ("PPID" 8 nil)
    ("User" 16 t)
    ("RSS" 8 ,(sysproc-make-numeric-sort-fn 3))
    ("PCPU" 6 ,(sysproc-make-numeric-sort-fn 4))
    ("Command" 60 t)]
  "Format for the tabulated list data - see `tabulated-list-format'.")

(defvar sysproc-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "d") 'sysproc-mark-process-for-deletion)
    (define-key map (kbd "s") 'sysproc-toggle-sort)
    (define-key map (kbd "u") 'sysproc-unmark-process)
    (define-key map (kbd "U") 'sysproc-unmark-all-processes)
    (define-key map (kbd "x") 'sysproc-do-marked-delete)
    map))

(defun sysproc--number-to-string (n)
  "As per `number-to-string' for N but converts nil to '?'."
  (if (numberp n)
      (number-to-string n)
    "?"))

(defun sysproc--get-process-list ()
  "Get the system process list.
Return a list of row items, each of which is a list begining with
a numeric identifier, followed by a vector of column entries,
each item of which is a string."
  (mapcar #'(lambda (pid)
	      (let* ((attribs (process-attributes pid))
		     (rss (alist-get 'rss attribs))
		     (pcpu (alist-get 'pcpu attribs))
		     (args (alist-get 'args attribs)))
		(list pid (vector (sysproc--number-to-string pid)
				  (sysproc--number-to-string (alist-get 'ppid attribs))
				  (alist-get 'user attribs)
				  (sysproc--number-to-string rss)
 				  (if (numberp pcpu) (format "%.2f" pcpu) "?")
				  ;; On macOS args will be nil.
				  (or args (alist-get 'comm attribs))))))
	  (list-system-processes)))

(defun sysproc-do-marked-delete ()
  "In Sysproc, kill processes marked for deletion.
By default will sent SIGINT to all marked processes.  If called
with a prefix argument then prompts the user for the signal to
send from the signals in `sysproc-signal-list'."
  (interactive)
  (let ((signalled 0)
	(sig (if current-prefix-arg
		 (intern (completing-read "Signal to send: "
					  sysproc-signal-list))
	       sysproc-default-signal)))
    (save-excursion
      (save-match-data
	(goto-char (point-min))
	(while (re-search-forward "^D \\([0-9]+\\)" nil t)
	  (signal-process (string-to-number (match-string 1)) sig)
	  (setq signalled (1+ signalled)))))
    (if (= signalled 0)
	(message "Couldn't find any processes to kill.")
      (message "Sent %s to %d process%s." sig signalled
	       (if (> signalled 1) "es" ""))
      (revert-buffer))))

(defun sysproc-mark-process-for-deletion ()
  "Mark the current process for deletion."
  (interactive)
  (tabulated-list-put-tag "D" t))

(defun sysproc--next-sort-index (index)
  "Return the next sort position after INDEX or nil if none."
  (catch 'exit
    (while t
      (setq index (1+ index))
      (when (= index (length sysproc-tabulated-list-format))
	(throw 'exit nil))
      (when (caddr (elt sysproc-tabulated-list-format index))
	(throw 'exit index)))))

(defun sysproc-toggle-sort ()
  "Toggle the sort criteria for the buffer.
Switches between unsorted, sorted by user, sorted by RSS, sorted
by PCPU, and sorted by command."
  (interactive)
  (setq sysproc-sort-field (sysproc--next-sort-index (or sysproc-sort-field -1)))
  (if sysproc-sort-field
      (let ((field-name (car (elt sysproc-tabulated-list-format sysproc-sort-field))))
	(setq tabulated-list-sort-key (cons field-name nil))
	(message "Sorting process list by %s" field-name))
    (setq tabulated-list-sort-key nil)
     (message "Process list is unsorted."))

  (tabulated-list-init-header)
  (tabulated-list-print))

(defun sysproc-unmark-process ()
  "Unmark the current process."
  (interactive)
   (tabulated-list-put-tag "" t))

;; Emacs 26.1 (as present in Debian 10 "buster") doesn't include this
;; function, so take the implementation from Emacs 28.0.50.
(unless (fboundp 'tabulated-list-clear-all-tags)
  (defun tabulated-list-clear-all-tags ()
    "Clear all tags from the padding area in the current buffer."
    (unless (> tabulated-list-padding 0)
      (error "There can be no tags in current buffer"))
    (save-excursion
      (goto-char (point-min))
      (let ((inhibit-read-only t)
            ;; Match non-space in the first n characters.
            (re (format "^ \\{0,%d\\}[^ ]" (1- tabulated-list-padding)))
            (empty (make-string tabulated-list-padding ? )))
	(while (re-search-forward re nil 'noerror)
          (tabulated-list-put-tag empty))))))

(defun sysproc-unmark-all-processes ()
  "Unmark all process."
  (interactive)
  (tabulated-list-clear-all-tags))

(define-derived-mode sysproc-mode
  tabulated-list-mode "Sysproc"
  "Major mode for listing system processes."
  ;; Note, can't use setq-local to set multiple variables in Emacs
  ;; 26.1.
  (setq-local tabulated-list-entries #'sysproc--get-process-list)
  (setq-local tabulated-list-format sysproc-tabulated-list-format)
  (setq-local tabulated-list-padding 2))

;;;###autoload
(defun sysproc ()
  "Display a list of system processes to the user.

\\<sysproc-mode-map>\ The order of the displayed processes can be
changed with \\[sysproc-toggle-sort] and cycles between unsorted,
sorted by user, sorted by RSS, sorted by PCPU, and sorted by
command.

Processes can be marked for deletion with
\\[sysproc-mark-process-for-deletion] and then killed with
\\[sysproc-do-marked-delete].  Use a prefix argument to be
prompted for the signal to send to the process.  The current
process can be unmarked with \\[sysproc-unmark-process], and all
marked processes unmarked with \\[sysproc-unmark-all-processes].

Type \\[describe-mode] after entering Sysproc for more info."
  (interactive)
  (let ((buffer (get-buffer-create sysproc-buffer-name)))
    (with-current-buffer buffer
      (read-only-mode -1)
      (erase-buffer))

    (switch-to-buffer buffer)
    (sysproc-mode)
    (tabulated-list-init-header)
    (tabulated-list-print)))

(provide 'sysproc)

;;; sysproc.el ends here
